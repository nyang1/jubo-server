
import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
    firstName: {
        type:String,
        trim: true
    },
    lastName: {
        type: String,
        trim: true
    },
    email: {
        type: String,
        required: true
    }
});


userSchema.pre('save', async (next)=>{
    this.createdAt = new Date();
    next();
});

export default mongoose.model('User', userSchema);
