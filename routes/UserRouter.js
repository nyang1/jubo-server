import Router from 'koa-router';
import User from '../models/User';


const userRouter = new Router();
userRouter.get('/users', async ctx=>{
    let users = await User.find({});
    ctx.body = users;
});

export default userRouter;

