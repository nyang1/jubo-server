
import Koa from 'koa';
import userRouter from './routes/UserRouter';
import {dbConfig} from './db/config';
import mongoose from 'mongoose';

const bodyParser = require('koa-bodyparser');
const app = new Koa();
const port = process.env.PORT || 5555;

mongoose.connect(dbConfig.url);
mongoose.connection.on('connected', ()=>{
    console.log('MongoDB connected');
})

app.use(bodyParser())
.use(userRouter.routes());

app.listen(port, ()=>{
    console.log('App is running');
});

